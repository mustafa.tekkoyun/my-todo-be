# Todo-List Project - Backend

This project is a todo list backend project. It works as a REST API. It works with `Python Flask.`

There are 2 services in the project.

**(GET)** `/api/todos`: Return all todos.

**(POST)** `/api/create-todo`: Return added todo. Client should send text in body. The service will reply with a new ID.

### Run
Clone this repository. You must have Python and Flask installed on your system.
`python3 app.py`

### Test
`python3 test.py`


##### Missings
I could not integrate gitlab-ci.yml in Flask. So I clone the project by connecting to digitalocean with ssh. I ran the service with Docker Container. I could not use pact.io.
   
