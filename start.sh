#!/bin/bash
app="cloudnesil"
docker stop ${app}
docker rm ${app}
docker build -t ${app} .
docker run -d -p 4000:80 \
  --name=${app} \
  -v $PWD:/app ${app}