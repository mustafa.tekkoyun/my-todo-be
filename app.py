from flask import Flask, jsonify, request

app = Flask(__name__)
todos = []

@app.route('/api/todos', methods=['GET'])
def get_todos():
    response = jsonify({'todos': todos})
    return response,200

@app.route('/api/create-todo',methods=["POST"])
def create_todo():
    todo = {
        'id': len(todos) + 1,
        'text': request.json['text'],
        'done': False
    }
    todos.append(todo)
    response = jsonify({'todo': todo})
    return response,201

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  return response

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=4000,debug=True)

