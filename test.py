import unittest
import app
import requests
import json
import sys

class TestFlaskApiUsingRequests(unittest.TestCase):
    # Create First Todo Item
    def test_create_todo(self):
        todo = {"text":"Item 1"}
        response = requests.post("http://localhost:4000/api/create-todo", data=json.dumps(todo), headers={'Content-Type': 'application/json'})
        self.assertEqual(response.json(), {'todo': {'done': False, 'id': 1, 'text': 'Item 1'}})

    # Create Second Todo Item
    def test_create_todo_for_second(self):
        todo = {"text":"Item 2"}
        response = requests.post("http://localhost:4000/api/create-todo", data=json.dumps(todo), headers={'Content-Type': 'application/json'})
        self.assertEqual(response.json(), {'todo': {'done': False, 'id': 2, 'text': 'Item 2'}})

    # TodoList Two Item
    def test_get_todos_for_second(self):
        response = requests.get('http://localhost:4000/api/todos')
        self.assertEqual(response.json(), {'todos': [{'done': False, 'id': 1, 'text': 'Item 1'},{'done': False, 'id': 2, 'text': 'Item 2'}]})
if __name__ == "__main__":
    unittest.main()
